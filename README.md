# Distributed System

To address the issues, the approach that I took is to use a message broker (RabbitMQ) to communicate between the two modules, order and voucher. This opens up the possibility to use any number of different technologies on either of the modules.

### Dirgram

![Scheme](files/diagram.png)

## Technoligies

* PHP - Lumen Framework
* Docker
* RabbitMQ - https://www.cloudamqp.com
* MySQL

## Installation

There are two additional repositories which hold the Order Module and Voucher Module. Setup instructions can be found on their respective repository README files.

* [Order Module](https://bitbucket.org/aharen/ordering-service)
* [Voucher Module](https://bitbucket.org/aharen/vourcher-service)

The `.env` files on the respositories will have default values that would mostly be enough for getting it up and running. The setup process is identical between both.

**NOTE:** I have added RabbitMQ keys to the `.env` file for ease of set up

## Usage

There is a single API endpoint exposed on the Order Service which accepts two parameters; email address and total order amount in cents.


```
METHOD: POST
URL: http://localhost:3000/order
PARAMS:

email: 
- valid email address
total: 
- total order amount in cents
```

When an order has been created (assumption is made here that it was fullfilled) orders that have `total` more than 100 (10000 in cents) fires an event which is dispatched to RabbitMQ

A queue worker is running on the Voucher Service, which listens to and validates and creates a voucher if necessary.

### Queue Demo:

![Scheme](files/queue-worker.gif)

[Video Demo](files/queue-worker.mov)

## Advantages

In my optinion, this approach provides a lot of advantages. 

* It is highly scalable and also can be scaled individually based on requirements.
* There is also room for module expansion, for example; using the voucher module for any type of vouchers.
* Dev stack and deployment pipelines can be changes per module and switched where necessary without affecting other modules.